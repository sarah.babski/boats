﻿using UnityEngine;

namespace Otrspc
{
    public class WaterPlaneVisualization : MonoBehaviour
    {
        [SerializeField]
        private GameObject visualization;

        private bool visible;
        private bool showable = false;

        public void Show()
        {
            if (!showable) return;
            Toggle(true);
        }

        public void Hide()
        {
            Toggle(false);
        }

        private void Toggle(bool b)
        {
            if (visible == b) return;
            visible = b;
            visualization.SetActive(b);
        }

        public void SetShowable(bool b)
        {
            showable = b;

            if (b)
            {
                Show();
            }
            else
            {
                Hide();
            }
        }
    }
}

