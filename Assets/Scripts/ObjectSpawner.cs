﻿using System.Collections.Generic;
using UnityEngine;

namespace Otrspc
{
    public class ObjectSpawner
    {
        private const int AmountToSpawn = 10;

        private readonly Stack<GameObject> available;
        private readonly Transform PoolRoot;
        private readonly GameObject Prefab;


        public ObjectSpawner(GameObject prefab, Transform root)
        {
            PoolRoot = root;
            Prefab = prefab;
            available = new Stack<GameObject>();
            CreateInitialPool();
        }

        private void CreateInitialPool()
        {
            for (int i = 0; i < AmountToSpawn; i++)
            {
                GameObject obj = Object.Instantiate(Prefab, PoolRoot);
                obj.SetActive(false);
                available.Push(obj);
            }
        }

        public GameObject RetrieveBoat()
        {
            if (available.Count == 0)
            {
                //TODO: create more than one?
                return Object.Instantiate(Prefab);
            }

            GameObject obj = available.Pop();
            obj.SetActive(true);
            obj.transform.parent = null;
            return obj;
        }

        public void ReturnBoat(GameObject obj)
        {
            obj.transform.SetParent(PoolRoot);
            obj.SetActive(false);
            available.Push(obj);
        }

    }

}