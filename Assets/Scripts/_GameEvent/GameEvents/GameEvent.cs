﻿using System.Collections.Generic;
using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class GameEvent : AGameEvent<IGameEventListener>
    {
        public void Raise()
        {
            ApplyRaiseToAll();
        }

        protected override void RaiseEvent(IGameEventListener listener)
        {
            listener.OnEventRaised();
        }
    }
}