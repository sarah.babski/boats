﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class FloatVariable : Variable
    {
        [SerializeField]
        private float _value;

        public float Value
        {
            get { return _value; }
            set
            {
                _value = value;
                RaiseEvent();
            }
        }
    }
}
