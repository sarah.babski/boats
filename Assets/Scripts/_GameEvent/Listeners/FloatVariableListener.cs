﻿using UnityEngine;

namespace Otrspc
{
    public class FloatVariableListener : MonoBehaviour, IGameEventListener
    {
        [SerializeField]
        private FloatVariable var;

        [SerializeField]
        private EventFV response;

        private void OnEnable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        public void OnEventRaised()
        {
            response?.Invoke(var);
        }
    }
}
