﻿using UnityEngine;

namespace Otrspc
{
    public class OpenDebug : MonoBehaviour
    {
        [SerializeField]
        private GameEvent openEvent;

        private const float timeOut = 0.5f;

        private float lastTap;
        private int count;
        private bool open;

        private void Update()
        {
            if(!open && Input.touchCount > 1)
            {
                if(Input.GetTouch(0).position.x < 100)
                {
                    open = true;
                    openEvent?.Raise();
                }
            }
        }

        public void Close()
        {
            open = false;
        }
    }
}
