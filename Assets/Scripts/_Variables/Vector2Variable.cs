﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class Vector2Variable : Variable
    {
        [SerializeField]
        private float _x;

        [SerializeField]
        private float _y;

        public float X
        {
            get { return _x; }
            set
            {
                _x = value;
                RaiseEvent();
            }
        }

        public float Y
        {
            get { return _y; }
            set
            {
                _y = value;
                RaiseEvent();
            }
        }
    }
}
