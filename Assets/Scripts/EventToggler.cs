﻿using UnityEngine;

namespace Otrspc
{
    public class EventToggler : MonoBehaviour
    {
        [SerializeField]
        private GameEvent onEvent;

        [SerializeField]
        private GameEvent offEvent;

        public void RaiseEvent(bool b)
        {
            if(b)
            {
                onEvent?.Raise();
            }
            else
            {
                offEvent?.Raise();
            }
        }
    }
}
