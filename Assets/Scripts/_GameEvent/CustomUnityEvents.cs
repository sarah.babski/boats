﻿using UnityEngine;
using UnityEngine.Events;

namespace Otrspc
{
    [System.Serializable]
    public class EventTV : UnityEvent<TransformVariable>
    {
    }

    [System.Serializable]
    public class EventGO : UnityEvent<GameObject>
    {
    }

    [System.Serializable]
    public class EventFloat : UnityEvent<float>
    {
    }

    [System.Serializable]
    public class EventFV : UnityEvent<FloatVariable>
    {
    }

    [System.Serializable]
    public class EventVector3 : UnityEvent<Vector3>
    {
    }
}
