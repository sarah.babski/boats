﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Otrspc
{
    public class Test : MonoBehaviour
    {
        private TextMeshProUGUI text;

        private void Awake()
        {
            text = GetComponentInChildren<TextMeshProUGUI>();
        }

        void Update()
        {
            GameObject g = EventSystem.current.currentSelectedGameObject;
            string message = g != null ? g.ToString() : "None";
            text.text = message;
        }
    }
}
