﻿using UnityEngine;
using UnityEngine.Events;

namespace Otrspc
{
    public class EventTester : AEventTester
    {
        [SerializeField]
        private UnityEvent eventToTest;

        public override void Test()
        {
            eventToTest?.Invoke();
        }
    }
}