﻿using UnityEngine;

namespace Otrspc
{
    public class FloatVariableChanger : MonoBehaviour
    {
        [SerializeField]
        private FloatVariable var;

        public void SetValue(float newVal)
        {
            var.Value = newVal;
        }

        public void AddValue(float addVal)
        {
            var.Value += addVal;
        }
    }
}
