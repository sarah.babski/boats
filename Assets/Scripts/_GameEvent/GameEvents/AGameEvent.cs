﻿using System.Collections.Generic;
using UnityEngine;

namespace Otrspc
{
    public abstract class AGameEvent<T> : ScriptableObject
    {
        private readonly List<T> Listeners = new List<T>();

        public void Subscribe(T listener)
        {
            if (!Listeners.Contains(listener)) Listeners.Add(listener);
        }

        public void Unsubscribe(T listener)
        {
            Listeners.Remove(listener);
        }

        protected void ApplyRaiseToAll()
        {
            for (int i = Listeners.Count - 1; i >= 0; i--)
            {
                RaiseEvent(Listeners[i]);
            }
        }

        protected abstract void RaiseEvent(T listener);
    }
}
