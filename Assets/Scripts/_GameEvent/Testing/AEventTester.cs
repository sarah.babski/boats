﻿using UnityEngine;

namespace Otrspc
{
    public abstract class AEventTester : MonoBehaviour
    {
        public abstract void Test();
    }
}
