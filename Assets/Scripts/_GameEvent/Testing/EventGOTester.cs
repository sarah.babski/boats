﻿using UnityEngine;

namespace Otrspc
{
    public class EventGOTester : AEventTester
    {
        [SerializeField]
        private GameObject testObject;

        [SerializeField]
        private EventGO eventToTest;

        public override void Test()
        {
            eventToTest?.Invoke(testObject);
        }
    }
}
