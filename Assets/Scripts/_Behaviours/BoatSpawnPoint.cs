﻿using UnityEngine;

namespace Otrspc
{
    public class BoatSpawnPoint : MonoBehaviour
    {
        [SerializeField]
        private float minSpawnTime = 30f;

        [SerializeField]
        private float maxSpawnTime = 60f;

        [SerializeField]
        private GameEventVector3 spawnEvent;

        private void Awake()
        {
            Invoke("Spawn", Random.Range(minSpawnTime, maxSpawnTime));
        }

        private void Spawn()
        {
            spawnEvent.Raise(transform.position);
            Invoke("Spawn", Random.Range(minSpawnTime, maxSpawnTime));
        }
    }
}
