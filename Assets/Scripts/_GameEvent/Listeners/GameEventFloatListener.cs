﻿using UnityEngine;

namespace Otrspc
{
    public class GameEventFloatListener : MonoBehaviour
    {
        [SerializeField]
        private GameEventFloat gameEvent;

        [SerializeField]
        private EventFloat response;

        private void OnEnable()
        {
            gameEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            gameEvent?.Unsubscribe(this);
        }

        public void OnEventRaised(float f)
        {
            response?.Invoke(f);
        }
    }
}


//vector2 variable spacing