﻿using UnityEngine;

namespace Otrspc
{
    public class GameEventGOListener : MonoBehaviour
    {
        [SerializeField]
        private GameEventGO gameEvent;

        [SerializeField]
        private EventGO response;

        private void OnEnable()
        {
            gameEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            gameEvent?.Unsubscribe(this);
        }

        public void OnEventRaised(GameObject go)
        {
            response?.Invoke(go);
        }
    }
}
