﻿using UnityEngine;

namespace Otrspc
{
    public class WishOrb : MonoBehaviour
    {
        [SerializeField]
        private GameEventGO collideEvent;

        [SerializeField]
        private TransformVariable orb;

        [SerializeField]

        private const float heightAbove = 5f;

        private ParticleSystem particles;

        private void Awake()
        {
            orb.Position = transform.position;
            particles = GetComponentInChildren<ParticleSystem>();
        }

        private void OnCollisionEnter(Collision other)
        {
            Boat b = other.transform.GetComponent<Boat>();
            if(b != null)
            {
                //TODO: more than one?
                particles.transform.position = other.contacts[0].point;
                particles.Play();
                collideEvent?.Raise(b.gameObject);
            }
        }

        public void SetHeight(TransformVariable variable)
        {
            Vector3 pos = transform.position;
            pos.y = variable.Position.y + heightAbove;
            transform.position = pos;
            orb.Position = transform.position;
        }

        public void SetX(FloatVariable variable)
        {
            Vector3 pos = transform.position;
            pos.x = variable.Value / 2;
            transform.position = pos;
            orb.Position = transform.position;
        }

        public void SetY(FloatVariable variable)
        {
            Vector3 pos = transform.position;
            pos.z = variable.Value / 2;
            transform.position = pos;
            orb.Position = transform.position;
        }
    }
}
