﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace Otrspc
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField]
        private UnityEvent inputEvent;

        private const float Cooldown = 2f;

        private float lastInput;

        private void OnEnable()
        {
            lastInput = Time.time - 2f;
        }

        private void DoInput()
        {
            inputEvent?.Invoke();
            lastInput = Time.time;
        }

        private void Update()
        {
            if (lastInput + Cooldown > Time.time) return;

#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0) && !HasSelectedObject())
            {
                DoInput();
            }
#else
            if (Input.touchCount > 0 && !HasSelectedObject())
            {
                DoInput();
            }
#endif
        }

        private bool HasSelectedObject()
        {
            return EventSystem.current.currentSelectedGameObject != null;
        }
    }
}
