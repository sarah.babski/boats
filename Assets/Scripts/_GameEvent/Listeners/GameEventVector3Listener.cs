﻿using UnityEngine;

namespace Otrspc
{
    public class GameEventVector3Listener : MonoBehaviour
    {

        [SerializeField]
        private GameEventVector3 gameEvent;

        [SerializeField]
        private EventVector3 response;

        private void OnEnable()
        {
            gameEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            gameEvent?.Unsubscribe(this);
        }

        public void OnEventRaised(Vector3 v)
        {
            response?.Invoke(v);
        }
    }
}
