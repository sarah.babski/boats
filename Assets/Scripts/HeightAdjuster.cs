﻿using UnityEngine;

namespace Otrspc
{
    public class HeightAdjuster : MonoBehaviour
    {
        [SerializeField]
        private TransformVariable var;

        [SerializeField]
        private float adjustAmt;

        public void Increase()
        {
            var.Position += Vector3.up * adjustAmt;
        }

        public void Decrease()
        {
            var.Position -= Vector3.up * adjustAmt;
        }
    }
}
