﻿using UnityEditor;
using UnityEngine;

namespace Otrspc
{
    [CustomEditor(typeof(AEventTester), true)]
    public class EventTesterEditor : Editor
    {
        private const string TestString = "Test";

		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			GUI.enabled = Application.isPlaying;

			AEventTester e = target as AEventTester;
			if (GUILayout.Button(TestString)) e.Test();
        }
	}
}
