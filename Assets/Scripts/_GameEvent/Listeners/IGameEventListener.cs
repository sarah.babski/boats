﻿namespace Otrspc
{
    public interface IGameEventListener
    {
        void OnEventRaised();
    }
}
