﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace Otrspc
{
    public class Boat : MonoBehaviour
    {
        [SerializeField]
        private EventGO destroyEvent;

        [SerializeField]
        private Vector2Variable waterMove;

        [SerializeField]
        private float lifetime = 300f;

        [SerializeField]
        private float targetRange = 2.5f;

        [SerializeField]
        private float verticalScale = 0.5f;

        private NavMeshAgent agent;
        private Rigidbody rb;
        private Quaternion targetRotation;
		private Vector3 startScale;
        private Vector3 origin;
        private Vector3 target;
        private float livingTime;
        private float scaleModifier;
        private bool moving;
        private bool orbTime;

        private Vector3 realTarget;

        private void Awake()
		{
			startScale = transform.localScale;
            agent = GetComponent<NavMeshAgent>();
            rb = GetComponent<Rigidbody>();
		}

        public void Initialize(Vector3 position, Quaternion rotation, Vector3 centerPoint, Vector3 targetPos)
        {
            livingTime = 0;
            moving = true;
            orbTime = false;
            origin = centerPoint;
            agent.enabled = true;
            rb.isKinematic = true;

            target = targetPos;

            transform.position = position;
            transform.rotation = rotation;

            Vector2 c = Random.insideUnitCircle * targetRange;
            realTarget = new Vector3(c.x + target.x, 0, c.y + target.z);
            agent.destination = realTarget;
            StartCoroutine(Scale(startScale / 100f, startScale));
        }

        private IEnumerator Scale(Vector3 start, Vector3 end)
        {
            float currentTime = 0;
            while(currentTime < 1)
            {
                transform.localScale = Vector3.Lerp(start, end, currentTime);
                currentTime += Time.deltaTime;
                yield return null;
            }

            transform.localScale = end;
        }

        private void OnDisable()
        {
            moving = false;
        }

        public void ChangeHeight(TransformVariable var)
        {
            if(moving) origin.y = var.Position.y;
        }

        public void ChangeScale(FloatVariable var)
		{
            scaleModifier = var.Value;
			transform.localScale = startScale * scaleModifier;
        }

        private void Update()
        {
            if (!moving) return;

            if (orbTime)
            {
                Vector3 forceDirection = (target - transform.position).normalized;
                rb.AddForce(forceDirection * Time.deltaTime, ForceMode.Impulse);
                return;
            }

            if (livingTime > lifetime)
            {
                orbTime = true;
                agent.enabled = false;
                rb.isKinematic = false;
                StartCoroutine(Scale(startScale, startScale / 100f));
            }

            MoveVert();

            livingTime += Time.deltaTime;
        }

        private void MoveVert()
        {
            Vector3 pos = transform.position;
            transform.position = SampleNoise(pos);

            transform.LookAt(SampleNoise(pos + transform.forward * 0.25f * transform.localScale.x));
        }

        private Vector3 SampleNoise(Vector3 point)
        {
            Vector3 v = new Vector2(point.x + waterMove.X, point.x + waterMove.Y);
            point.y = Mathf.PerlinNoise(v.x, v.y) * verticalScale + origin.y;
            return point;
        }

        public void Destroy()
        {
            destroyEvent?.Invoke(gameObject);
        }

        public void SetTarget(TransformVariable variable)
        {
            target = variable.Position;
        }
    }
}