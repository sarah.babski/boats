﻿using UnityEngine;

namespace Otrspc
{
    public class TransformVariableListener : MonoBehaviour, IGameEventListener
    {
        [SerializeField]
        private TransformVariable var;

        [SerializeField]
        private EventTV response;

        private void OnEnable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        public void OnEventRaised()
        {
            response?.Invoke(var);
        }
    }
}
