﻿using UnityEngine;

namespace Otrspc
{
    public class BoatPool : MonoBehaviour
    {
        [SerializeField]
        private GameObject boatPrefab;

        [SerializeField]
        private Vector2Variable waterMove;

        [SerializeField]
        private float waterSpeed = 1;

        [SerializeField]
        private Transform target;

        private ObjectSpawner spawner;

        private void Awake()
        {
            spawner = new ObjectSpawner(boatPrefab, transform);
        }

        public void SetTransform(TransformVariable var)
		{
            //TODO: more nuanced? if not, replace with transform follower
            transform.position = var.Position;
            transform.rotation = var.Rotation;
		}

        public void CreateBoat()
        {
            CreateBoat(transform.position);
        }

        public void CreateBoat(Vector3 position)
        {
            Boat boat = spawner.RetrieveBoat().GetComponent<Boat>();
            boat.Initialize(position, transform.rotation, transform.position, target.position);
        }

        public void DestroyBoat(GameObject go)
        {
            spawner.ReturnBoat(go);
        }

        private void Update()
        {
            waterMove.X += Time.deltaTime * waterSpeed;
        }
    }
}