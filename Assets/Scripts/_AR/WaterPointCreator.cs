﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Otrspc
{
    public class WaterPointCreator : MonoBehaviour
    {
        [SerializeField]
        private TransformVariable plane;

        [SerializeField]
        private GameEvent createEvent;

        private static List<ARRaycastHit> hits = new List<ARRaycastHit>();

        private ARRaycastManager raycastManager;

        private bool placed;

        private void Awake()
        {
            raycastManager = GetComponent<ARRaycastManager>();
        }

        public void Reset()
        {
            placed = false;
        }

        private bool TryGetTouchPosition(out Vector2 touchPosition)
        {
#if UNITY_EDITOR
            if (Input.GetMouseButtonDown(0) && !HasSelectedObject())
            {
                var mousePosition = Input.mousePosition;
                touchPosition = new Vector2(mousePosition.x, mousePosition.y);
                return true;
            }
#else
            if (Input.touchCount > 0 && !HasSelectedObject())
            {
                touchPosition = Input.GetTouch(0).position;
                return true;
            }
#endif
            touchPosition = default;
            return false;
        }

        private bool HasSelectedObject()
        {
            return EventSystem.current.currentSelectedGameObject != null;
        }

        private void Update()
        {
            if (placed) return;

            if (!TryGetTouchPosition(out Vector2 touchPosition)) return;

            if (raycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinInfinity))
            {
                Pose hitPose = hits[0].pose;

                Vector3 rotation = hitPose.rotation.eulerAngles;
                rotation.y = GetComponent<ARSessionOrigin>().camera.transform.eulerAngles.y;
                plane.SetPositionAndRotation(hitPose.position, Quaternion.Euler(rotation));
                createEvent?.Raise();
                placed = true;
            }
        }
    }
}
