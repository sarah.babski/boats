﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class GameEventVector3 : AGameEvent<GameEventVector3Listener>
    {
        private Vector3 currentValue;

        public void Raise(Vector3 v)
        {
            currentValue = v;
            ApplyRaiseToAll();
        }

        protected override void RaiseEvent(GameEventVector3Listener listener)
        {
            listener.OnEventRaised(currentValue);
        }
    }
}
