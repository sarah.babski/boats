﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class TransformVariable : Variable
    {
        [SerializeField]
        private Vector3 _position;

        [SerializeField]
        private Quaternion _rotation;

        public Vector3 Position
        {
            get { return _position; }

            set
            {
                _position = value;
                RaiseEvent();
            }
        }

        public Vector3 EulerAngles
        {
            get { return _rotation.eulerAngles; }

            set
            {
                _rotation = Quaternion.Euler(value);
                RaiseEvent();
            }
        }

        public Quaternion Rotation
        {
            get { return _rotation; }

            set
            {
                _rotation = value;
                RaiseEvent();
            }
        }

        public void SetPositionAndRotation(Vector3 position, Quaternion rotation)
        {
            _rotation = rotation;
            _position = position;
            RaiseEvent();
        }
    }
}
