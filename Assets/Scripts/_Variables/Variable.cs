﻿using UnityEngine;

namespace Otrspc
{
    public abstract class Variable : ScriptableObject
    {
        [SerializeField]
        private GameEvent _changeEvent;

        public GameEvent ChangeEvent
        {
            get { return _changeEvent; }
        }

        protected void RaiseEvent()
        {
            _changeEvent?.Raise();
        }
    }
}
