﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class GameEventFloat : AGameEvent<GameEventFloatListener>
    {
        private float currentValue;

        public void Raise(float f)
        {
            currentValue = f;
            ApplyRaiseToAll();
        }

        protected override void RaiseEvent(GameEventFloatListener listener)
        {
            listener.OnEventRaised(currentValue);
        }
    }
}
