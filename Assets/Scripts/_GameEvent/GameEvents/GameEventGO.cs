﻿using UnityEngine;

namespace Otrspc
{
    [CreateAssetMenu]
    public class GameEventGO : AGameEvent<GameEventGOListener>
    {
        private GameObject currentValue;

        public void Raise(GameObject go)
        {
            currentValue = go;
            ApplyRaiseToAll();
        }

        protected override void RaiseEvent(GameEventGOListener listener)
        {
            listener.OnEventRaised(currentValue);
        }
    }
}
