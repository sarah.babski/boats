﻿using UnityEngine;

namespace Otrspc
{
    public class TransformVariableFollower : MonoBehaviour, IGameEventListener
    {
        [SerializeField]
        private TransformVariable var;

        private void OnEnable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        private void OnDisable()
        {
            var.ChangeEvent?.Subscribe(this);
        }

        public void OnEventRaised()
        {
            transform.position = var.Position;
            transform.rotation = var.Rotation;
        }
    }
}
