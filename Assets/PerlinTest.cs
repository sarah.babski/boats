﻿using UnityEngine;

namespace Otrspc
{
    public class PerlinTest : MonoBehaviour
    {
        private int width = 512;
        private int height = 512;

        public float xOrg;
        public float yOrg = 0;
        public float scale = 1;

        public float orgScale = 1;
        public float speed = 1;

        private Texture2D noiseTex;
        private Color[] pix;
        private Renderer rend;

        private float nextX;
        private float nextY;

        private void Awake()
        {
            rend = GetComponent<Renderer>();
            noiseTex = new Texture2D(width, height);
            pix = new Color[width * height];
            rend.material.mainTexture = noiseTex;
        }

        private void Update()
        {
            CalcNoise();

            /*xOrg = Mathf.Lerp(xOrg, nextX, Time.deltaTime * speed);
            if(Mathf.Abs(xOrg - nextX) < 0.05f)
            {
                nextX = xOrg + Random.Range(0f, 2f) * orgScale;
            }*/

            /*xOrg += Time.deltaTime * speed;

            yOrg = Mathf.Lerp(yOrg, nextY, Time.deltaTime * speed);
            if(Mathf.Abs(yOrg - nextY) < 0.05f)
            {
                nextY = yOrg + Random.Range(0f, 1f) * orgScale;
            }*/
        }

        private void CalcNoise()
        {
            float y = 0;
            while(y < height)
            {
                float x = 0;
                float yCoord = yOrg + y / height * scale;
                while(x < width)
                {
                    float xCoord = xOrg + x / width * scale;
                    float sample = Mathf.PerlinNoise(xCoord, yCoord);
                    pix[(int)(y * width + x)] = new Color(sample, sample, sample);
                    //waves.value[(int)(y * width + x)] = sample;
                    x++;
                }
                y++;
            }

            noiseTex.SetPixels(pix);
            noiseTex.Apply();
        }
    }
}
