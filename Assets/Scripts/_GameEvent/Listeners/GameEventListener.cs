﻿using UnityEngine;
using UnityEngine.Events;
namespace Otrspc
{
	public class GameEventListener : MonoBehaviour, IGameEventListener
	{
		[SerializeField]
		private GameEvent gameEvent;

		[SerializeField]
		private UnityEvent response;

		private void OnEnable()
		{
			gameEvent?.Subscribe(this);
		}

		private void OnDisable()
		{
			gameEvent?.Unsubscribe(this);
		}

        public void OnEventRaised()
        {
            response?.Invoke();
        }
    }
}